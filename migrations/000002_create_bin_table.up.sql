CREATE TYPE bin_status AS ENUM ('pending', 'available', 'unavailable');

CREATE TABLE IF NOT EXISTS bins(
  id VARCHAR(36) PRIMARY KEY,
  bin_name VARCHAR (255) NOT NULL,
  bin_description VARCHAR (255) NOT NULL,
  bin_status bin_status DEFAULT 'available' NOT NULL,
  bin_peripheral VARCHAR (255) NOT NULL,
  bin_latitude FLOAT NOT NULL,
  bin_longitude FLOAT NOT NULL
);