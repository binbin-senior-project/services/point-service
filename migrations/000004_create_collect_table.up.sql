CREATE TABLE IF NOT EXISTS collects(
  id SERIAL PRIMARY KEY,
  user_id VARCHAR(36),
  trash_id VARCHAR(36),
  created_at TIMESTAMP default CURRENT_TIMESTAMP,
  FOREIGN KEY (user_id) REFERENCES users (id),
  FOREIGN KEY (trash_id) REFERENCES trashes (id)
);