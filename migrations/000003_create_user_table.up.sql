CREATE TABLE IF NOT EXISTS users(
  id VARCHAR(36) PRIMARY KEY,
  user_firstname VARCHAR (255),
  user_lastname VARCHAR (255),
  user_phone VARCHAR (10),
  user_point REAL DEFAULT 0
);