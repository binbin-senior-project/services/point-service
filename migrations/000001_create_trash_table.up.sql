CREATE TABLE IF NOT EXISTS trashes(
  id VARCHAR(36) PRIMARY KEY,
  trash_name VARCHAR (255) NOT NULL,
  trash_code VARCHAR (255) NOT NULL,
  trash_regular_point REAL NOT NULL,
  trash_sale_point REAL NOT NULL
);