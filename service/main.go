package main

import (
	"github.com/joho/godotenv"

	db "gitlab.com/mangbinbin/services/point-service/service/database"
	amqp "gitlab.com/mangbinbin/services/point-service/service/delivery/amqp"
	grpc "gitlab.com/mangbinbin/services/point-service/service/delivery/grpc"
	r "gitlab.com/mangbinbin/services/point-service/service/repository"
	u "gitlab.com/mangbinbin/services/point-service/service/usecase"

	_ "github.com/lib/pq"
)

func main() {
	// For development purpose
	godotenv.Load()
	database := db.NewConnectDatabase()

	repo := r.RegisterRepository{
		Analytic: r.NewAnalyticRepository(database),
		Bin:      r.NewBinRepository(database),
		Trash:    r.NewTrashRepository(database),
		User:     r.NewUserRepository(database),
		Collect:  r.NewCollectRepository(database),
	}

	// Register Broker Client
	ch := amqp.NewPointDelivery()
	// Register Subscriber broker
	amqp.NewAMQPSubscribe(repo, ch)
	// Register Publisher broker
	broker := amqp.NewAMQPPublish(ch)

	usecase := u.RegisterUsecase{
		Analytic: u.NewAnalyticUsecase(repo, broker),
		Bin:      u.NewBinUsecase(repo, broker),
		Trash:    u.NewTrashUsecase(repo, broker),
		User:     u.NewUserUsecase(repo, broker),
	}

	// Register GRPC Server
	grpc.NewPointDelivery(usecase)
}
