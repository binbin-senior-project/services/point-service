package grpc

import (
	"context"

	pb "gitlab.com/mangbinbin/services/point-service/service/delivery/grpc/proto"
	dm "gitlab.com/mangbinbin/services/point-service/service/domain"
)

// CreateBin method
func (d *PointDelivery) CreateBin(ctx context.Context, req *pb.CreateBinRequest) (*pb.CreateBinReply, error) {
	bin := dm.Bin{
		Name:        req.Name,
		Description: req.Description,
		Peripheral:  req.Peripheral,
		Latitude:    req.Latitude,
		Longitude:   req.Longitude,
		Status:      req.Status,
	}

	err := d.usecase.Bin.Store(bin)

	if err != nil {
		return &pb.CreateBinReply{Success: false}, err
	}

	return &pb.CreateBinReply{Success: true}, nil
}

// GetBins method
func (d *PointDelivery) GetBins(ctx context.Context, req *pb.GetBinsRequest) (*pb.GetBinsReply, error) {
	bins, err := d.usecase.Bin.Fetch(int(req.Offset), int(req.Limit))

	if err != nil {
		return &pb.GetBinsReply{}, err
	}

	var rspBins []*pb.Bin

	for _, bin := range bins {
		rspBins = append(rspBins, &pb.Bin{
			Id:          bin.ID,
			Name:        bin.Name,
			Description: bin.Description,
			Peripheral:  bin.Peripheral,
			Status:      bin.Status,
			Latitude:    bin.Latitude,
			Longitude:   bin.Longitude,
		})
	}

	return &pb.GetBinsReply{Bins: rspBins}, nil
}

// GetBin method
func (d *PointDelivery) GetBin(ctx context.Context, req *pb.GetBinRequest) (*pb.GetBinReply, error) {
	bin, err := d.usecase.Bin.GetByID(req.BinID)

	if err != nil {
		return &pb.GetBinReply{}, err
	}

	rspBin := &pb.Bin{
		Id:          bin.ID,
		Name:        bin.Name,
		Description: bin.Description,
		Peripheral:  bin.Peripheral,
		Status:      bin.Status,
		Latitude:    bin.Latitude,
		Longitude:   bin.Longitude,
	}

	return &pb.GetBinReply{Bin: rspBin}, nil
}

// UpdateBin method
func (d *PointDelivery) UpdateBin(ctx context.Context, req *pb.UpdateBinRequest) (*pb.UpdateBinReply, error) {
	bin := dm.Bin{
		ID:          req.BinID,
		Name:        req.Name,
		Description: req.Description,
		Peripheral:  req.Peripheral,
		Status:      req.Status,
		Latitude:    req.Latitude,
		Longitude:   req.Longitude,
	}

	err := d.usecase.Bin.Update(bin)

	if err != nil {
		return &pb.UpdateBinReply{Success: false}, err
	}

	return &pb.UpdateBinReply{Success: true}, nil
}

// DeleteBin method
func (d *PointDelivery) DeleteBin(ctx context.Context, req *pb.DeleteBinRequest) (*pb.DeleteBinReply, error) {
	err := d.usecase.Bin.Delete(req.BinID)

	if err != nil {
		return &pb.DeleteBinReply{Success: false}, err
	}

	return &pb.DeleteBinReply{Success: true}, err
}
