package grpc

import (
	"context"

	pb "gitlab.com/mangbinbin/services/point-service/service/delivery/grpc/proto"
)

// GetNewTrashToday method
func (d *PointDelivery) GetNewTrashToday(ctx context.Context, req *pb.GetNewTrashTodayRequest) (*pb.GetNewTrashTodayReply, error) {
	count, err := d.usecase.Analytic.GetNewTrashToday()

	if err != nil {
		return &pb.GetNewTrashTodayReply{}, err
	}

	return &pb.GetNewTrashTodayReply{
		Count: int32(count),
	}, nil
}
