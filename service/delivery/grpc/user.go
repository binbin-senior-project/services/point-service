package grpc

import (
	"context"

	pb "gitlab.com/mangbinbin/services/point-service/service/delivery/grpc/proto"
)

// CollectPoint method
func (d *PointDelivery) CollectPoint(ctx context.Context, req *pb.CollectPointRequest) (*pb.CollectPointReply, error) {
	err := d.usecase.User.CollectPoint(req.UserID, req.TrashCodes)

	// return with error
	if err != nil {
		return &pb.CollectPointReply{Success: false}, err
	}

	// return with success
	return &pb.CollectPointReply{Success: true}, nil
}

// TransferPoint method
func (d *PointDelivery) TransferPoint(ctx context.Context, req *pb.TransferPointRequest) (*pb.TransferPointReply, error) {
	var (
		userID = req.UserID
		phone  = req.Phone
		point  = req.Point
	)

	// Transfer Points
	err := d.usecase.User.TransferPoint(userID, phone, point)

	if err != nil {
		return &pb.TransferPointReply{Success: false}, err
	}

	// No error in this case
	return &pb.TransferPointReply{Success: true}, nil
}
