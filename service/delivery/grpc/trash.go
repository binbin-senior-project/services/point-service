package grpc

import (
	"context"

	pb "gitlab.com/mangbinbin/services/point-service/service/delivery/grpc/proto"
	dm "gitlab.com/mangbinbin/services/point-service/service/domain"
)

// CreateTrash method
func (d *PointDelivery) CreateTrash(ctx context.Context, req *pb.CreateTrashRequest) (*pb.CreateTrashReply, error) {
	trash := dm.Trash{
		Name:         req.Name,
		Code:         req.Code,
		RegularPoint: req.RegularPoint,
		SalePoint:    req.SalePoint,
	}

	err := d.usecase.Trash.Store(trash)

	if err != nil {
		return &pb.CreateTrashReply{Success: false}, err
	}

	return &pb.CreateTrashReply{Success: true}, nil
}

// GetTrashes method
func (d *PointDelivery) GetTrashes(ctx context.Context, req *pb.GetTrashesRequest) (*pb.GetTrashesReply, error) {
	trashes, err := d.usecase.Trash.Fetch(int(req.Offset), int(req.Limit))

	if err != nil {
		return &pb.GetTrashesReply{}, err
	}

	var rspTrashes []*pb.Trash

	for _, trash := range trashes {
		rspTrashes = append(rspTrashes, &pb.Trash{
			Id:           trash.ID,
			Name:         trash.Name,
			Code:         trash.Code,
			RegularPoint: trash.RegularPoint,
			SalePoint:    trash.SalePoint,
		})
	}

	return &pb.GetTrashesReply{Trashes: rspTrashes}, nil
}

// GetTrashByCode method
func (d *PointDelivery) GetTrashByCode(ctx context.Context, req *pb.GetTrashByCodeRequest) (*pb.GetTrashByCodeReply, error) {
	trash, err := d.usecase.Trash.GetByCode(req.Code)

	if err != nil {
		return &pb.GetTrashByCodeReply{}, err
	}

	return &pb.GetTrashByCodeReply{
		Trash: &pb.Trash{
			Id:           trash.ID,
			Name:         trash.Name,
			Code:         trash.Code,
			RegularPoint: trash.RegularPoint,
			SalePoint:    trash.SalePoint,
		},
	}, nil
}

// GetTrash method
func (d *PointDelivery) GetTrash(ctx context.Context, req *pb.GetTrashRequest) (*pb.GetTrashReply, error) {
	trash, err := d.usecase.Trash.GetByID(req.TrashID)

	if err != nil {
		return &pb.GetTrashReply{}, err
	}

	rspTrash := &pb.Trash{
		Id:           trash.ID,
		Name:         trash.Name,
		Code:         trash.Code,
		RegularPoint: trash.RegularPoint,
		SalePoint:    trash.SalePoint,
	}

	return &pb.GetTrashReply{Trash: rspTrash}, nil
}

// UpdateTrash method
func (d *PointDelivery) UpdateTrash(ctx context.Context, req *pb.UpdateTrashRequest) (*pb.UpdateTrashReply, error) {
	trash := dm.Trash{
		ID:           req.TrashID,
		Name:         req.Name,
		Code:         req.Code,
		RegularPoint: req.RegularPoint,
		SalePoint:    req.SalePoint,
	}

	err := d.usecase.Trash.Update(trash)

	if err != nil {
		return &pb.UpdateTrashReply{Success: false}, err
	}

	return &pb.UpdateTrashReply{Success: true}, nil
}

// DeleteTrash method
func (d *PointDelivery) DeleteTrash(ctx context.Context, req *pb.DeleteTrashRequest) (*pb.DeleteTrashReply, error) {
	err := d.usecase.Trash.Delete(req.TrashID)

	if err != nil {
		return &pb.DeleteTrashReply{Success: false}, err
	}

	return &pb.DeleteTrashReply{Success: true}, err
}
