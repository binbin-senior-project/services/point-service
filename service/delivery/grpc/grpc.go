package grpc

import (
	"log"
	"net"
	"os"

	pb "gitlab.com/mangbinbin/services/point-service/service/delivery/grpc/proto"
	usecase "gitlab.com/mangbinbin/services/point-service/service/usecase"
	"google.golang.org/grpc"
)

// PointDelivery Holayy!
type PointDelivery struct {
	usecase usecase.RegisterUsecase
}

// NewPointDelivery method
func NewPointDelivery(usecase usecase.RegisterUsecase) {
	// Register protobuf handler with AuthgRPCDelievery struct
	delivery := &PointDelivery{
		usecase: usecase,
	}

	port := os.Getenv("SERVICE_PORT")

	// Listen out
	log.Println("Listen on port: " + port)

	// Create TCP Connection
	lis, err := net.Listen("tcp", ":"+port)

	// Detect connection error
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	// Create GRPC service
	s := grpc.NewServer()

	// Register GRPC to protobuffer
	pb.RegisterPointServiceServer(s, delivery)

	// Register TCP connection to GRPC
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Failed to serve: %v", err)
	}
}
