package amqp

import (
	"fmt"

	amqp "github.com/streadway/amqp"
	dm "gitlab.com/mangbinbin/services/point-service/service/domain"
	h "gitlab.com/mangbinbin/services/point-service/service/helper"
	r "gitlab.com/mangbinbin/services/point-service/service/repository"
)

// AMQPSubscribe struct
type AMQPSubscribe struct {
	repo    r.RegisterRepository
	channel *amqp.Channel
}

// NewAMQPSubscribe method
func NewAMQPSubscribe(repo r.RegisterRepository, channel *amqp.Channel) {
	subscriber := AMQPSubscribe{
		repo:    repo,
		channel: channel,
	}

	go subscriber.SubscribeUserCreatedEvent()
	go subscriber.SubscribeUserUpdatedEvent()
	go subscriber.SubscribePointUpdateEvent()
}

// SubscribeUserCreatedEvent subscribe
func (d *AMQPSubscribe) SubscribeUserCreatedEvent() {
	msgs, err := d.channel.Consume(
		"create.user@point.queue", // queue
		"",                        // consumer
		true,                      // auto-ack
		false,                     // exclusive
		false,                     // no-local
		false,                     // no-wait
		nil,                       // args
	)

	failOnError(err, "Failed to register a SubscribeRegistedEvent consumer")

	forever := make(chan bool)

	go func() {
		for msg := range msgs {
			u := dm.UserCreatedEvent{}
			err = h.DeserializePayload(&u, msg.Body)

			user := dm.User{
				ID:        u.UserID,
				Firstname: u.Firstname,
				Lastname:  u.Lastname,
				Phone:     u.Phone,
			}

			d.repo.User.Store(user)
		}
	}()

	<-forever
}

// SubscribeUserUpdatedEvent subscribe
func (d *AMQPSubscribe) SubscribeUserUpdatedEvent() {
	msgs, err := d.channel.Consume(
		"update.user@point.queue", // queue
		"",                        // consumer
		true,                      // auto-ack
		false,                     // exclusive
		false,                     // no-local
		false,                     // no-wait
		nil,                       // args
	)

	failOnError(err, "Failed to register a SubscribeRegistedEvent consumer")

	forever := make(chan bool)

	go func() {
		for msg := range msgs {
			u := dm.UserCreatedEvent{}
			err = h.DeserializePayload(&u, msg.Body)

			user := dm.User{
				ID:        u.UserID,
				Firstname: u.Firstname,
				Lastname:  u.Lastname,
				Phone:     u.Phone,
			}

			d.repo.User.Update(user)
		}
	}()

	<-forever
}

// SubscribePointUpdateEvent method
func (d *AMQPSubscribe) SubscribePointUpdateEvent() {
	msgs, err := d.channel.Consume(
		"update.point@point.queue", // queue
		"",                         // consumer
		true,                       // auto-ack
		false,                      // exclusive
		false,                      // no-local
		false,                      // no-wait
		nil,                        // args
	)

	failOnError(err, "Failed to register a SubscribePointUpdateEvent consumer")

	forever := make(chan bool)

	go func() {
		for msg := range msgs {
			point := dm.PointUpdatedEvent{}
			err = h.DeserializePayload(&point, msg.Body)

			d.repo.User.UpdateByKey(point.UserID, "user_point", fmt.Sprintf("%f", point.Point))
		}
	}()

	<-forever
}
