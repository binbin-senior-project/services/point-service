package amqp

import (
	"log"

	amqp "github.com/streadway/amqp"
	dm "gitlab.com/mangbinbin/services/point-service/service/domain"
	h "gitlab.com/mangbinbin/services/point-service/service/helper"
)

// AMQPPublish Holayy!
type AMQPPublish struct {
	channel *amqp.Channel
}

// NewAMQPPublish constructor
func NewAMQPPublish(channel *amqp.Channel) *AMQPPublish {
	return &AMQPPublish{
		channel: channel,
	}
}

func (d *AMQPPublish) publish(exchange string, route string, payload []byte) {
	err := d.channel.Publish(
		exchange, // exchange
		route,    // routing key
		false,    // mandatory
		false,    // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        payload,
		})

	if err != nil {
		log.Fatal(err)
	}
}

// PointUpdatedEvent method
func (d *AMQPPublish) PointUpdatedEvent(user dm.PointUpdatedEvent) {
	payload, err := h.SerializePayload(user)

	if err != nil {
		log.Fatal(err)
	}

	d.publish("point", "point.event.updated", payload)
}

// CreateUserTransaction method
func (d *AMQPPublish) CreateUserTransaction(transaction dm.UserTransactionCreatedEvent) {
	payload, err := h.SerializePayload(transaction)

	if err != nil {
		log.Fatal(err)
	}

	d.publish("transaction", "transaction.user.created", payload)
}

// CreateNotification method
func (d *AMQPPublish) CreateNotification(notification dm.NotificationCreatedEvent) {
	payload, err := h.SerializePayload(notification)

	if err != nil {
		log.Fatal(err)
	}

	d.publish("notification", "notification.event.created", payload)
}
