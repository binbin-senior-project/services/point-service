package usecase

import (
	amqp "gitlab.com/mangbinbin/services/point-service/service/delivery/amqp"
	dm "gitlab.com/mangbinbin/services/point-service/service/domain"
	r "gitlab.com/mangbinbin/services/point-service/service/repository"
)

// ITrashUsecase interface
type ITrashUsecase interface {
	Store(trash dm.Trash) error
	Fetch(offset int, limit int) ([]dm.Trash, error)
	GetByID(id string) (dm.Trash, error)
	GetByCode(code string) (dm.Trash, error)
	Update(trash dm.Trash) error
	Delete(id string) error
}

// TrashUsecase is an object
type TrashUsecase struct {
	repo   r.RegisterRepository
	broker *amqp.AMQPPublish
}

// NewTrashUsecase is an inital method for TrashUsecase struct
// Called by Client (main.go)
func NewTrashUsecase(repo r.RegisterRepository, broker *amqp.AMQPPublish) ITrashUsecase {
	return &TrashUsecase{
		repo:   repo,
		broker: broker,
	}
}

// Store method is a method
func (u *TrashUsecase) Store(trash dm.Trash) error {

	err := u.repo.Trash.Store(trash)

	return err
}

// Fetch method is a method
func (u *TrashUsecase) Fetch(offset int, limit int) ([]dm.Trash, error) {

	trashes, err := u.repo.Trash.Fetch(offset, limit)

	return trashes, err
}

// GetByID method is a method
func (u *TrashUsecase) GetByID(id string) (dm.Trash, error) {

	trash, err := u.repo.Trash.GetByID(id)

	return trash, err
}

// Update is a method
func (u *TrashUsecase) Update(trash dm.Trash) error {

	err := u.repo.Trash.Update(trash)

	return err
}

// Delete is a method
func (u *TrashUsecase) Delete(id string) error {

	err := u.repo.Trash.Delete(id)

	return err
}

// GetByCode is a method
func (u *TrashUsecase) GetByCode(code string) (dm.Trash, error) {

	trash, err := u.repo.Trash.GetByCode(code)

	return trash, err
}
