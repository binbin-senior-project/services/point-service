package usecase

import (
	"errors"
	"fmt"

	amqp "gitlab.com/mangbinbin/services/point-service/service/delivery/amqp"
	dm "gitlab.com/mangbinbin/services/point-service/service/domain"
	r "gitlab.com/mangbinbin/services/point-service/service/repository"
)

// IUserUsecase interface
type IUserUsecase interface {
	Store(user dm.User) error
	GetByPhone(phone string) (dm.User, error)
	CollectPoint(id string, codes []string) error
	TransferPoint(id string, phone string, point float64) error
}

// UserUsecase is an object
type UserUsecase struct {
	repo   r.RegisterRepository
	broker *amqp.AMQPPublish
}

// NewUserUsecase is an inital method for UserUsecase struct
// Called by Client (main.go)
func NewUserUsecase(repo r.RegisterRepository, broker *amqp.AMQPPublish) IUserUsecase {
	return &UserUsecase{
		repo:   repo,
		broker: broker,
	}
}

// Store method is a method
func (u *UserUsecase) Store(user dm.User) error {

	err := u.repo.User.Store(user)

	return err
}

// GetByPhone method is a method
func (u *UserUsecase) GetByPhone(phone string) (dm.User, error) {

	user, err := u.repo.User.GetByPhone(phone)

	return user, err
}

// CollectPoint is a method
func (u *UserUsecase) CollectPoint(id string, codes []string) error {

	var point float64

	for _, code := range codes {

		trash, err := u.repo.Trash.GetByCode(code)

		if err != nil {
			return err
		}

		point += trash.SalePoint
	}

	user, err := u.repo.User.GetByID(id)

	if err != nil {
		return err
	}

	up := float64(user.Point) + point

	err = u.repo.User.UpdateByKey(id, "user_point", fmt.Sprintf("%.2f", up))

	if err != nil {
		return err
	}

	for _, code := range codes {

		trash, err := u.repo.Trash.GetByCode(code)

		err = u.repo.Collect.Store(dm.Collect{
			UserID:  id,
			TrashID: trash.ID,
		})

		if err != nil {
			return err
		}
	}

	// Update point to other services
	go u.broker.PointUpdatedEvent(dm.PointUpdatedEvent{
		UserID: user.ID,
		Point:  up,
	})

	// Update point to other services
	go u.broker.CreateUserTransaction(dm.UserTransactionCreatedEvent{
		UserID:      user.ID,
		Type:        "collect",
		Point:       point,
		Description: "ได้รับแต้มจากการทิ้งขยะ",
	})

	// CreateNotification to notification service
	go u.broker.CreateNotification(dm.NotificationCreatedEvent{
		UserID: user.ID,
		Title:  "ทิ้งขยะ",
		Body:   "คุณได้รับแต้มจำนวน " + fmt.Sprintf("%.2f", point) + " แต้ม",
	})

	return nil
}

// TransferPoint is a method
func (u *UserUsecase) TransferPoint(id string, phone string, point float64) error {

	user, err := u.repo.User.GetByID(id)

	if err != nil {
		return err
	}

	receiver, err := u.repo.User.GetByPhone(phone)

	if err != nil {
		return err
	}

	if user.Point < point {
		return errors.New("Point is not enough!")
	}

	up := user.Point - point
	rp := receiver.Point + point

	err = u.repo.User.UpdateByKey(user.ID, "user_point", fmt.Sprintf("%.2f", up))

	if err != nil {
		return err
	}

	err = u.repo.User.UpdateByKey(receiver.ID, "user_point", fmt.Sprintf("%.2f", rp))

	if err != nil {
		return err
	}

	// Update point to other services
	go u.broker.PointUpdatedEvent(dm.PointUpdatedEvent{
		UserID: user.ID,
		Point:  up,
	})

	go u.broker.PointUpdatedEvent(dm.PointUpdatedEvent{
		UserID: receiver.ID,
		Point:  rp,
	})

	// Update point to other services
	go u.broker.CreateUserTransaction(dm.UserTransactionCreatedEvent{
		UserID:      user.ID,
		Type:        "transfer",
		Point:       point,
		Description: "มอบแต้มให้คุณ" + receiver.Firstname + " " + receiver.Lastname,
	})

	// Update point to other services
	go u.broker.CreateUserTransaction(dm.UserTransactionCreatedEvent{
		UserID:      receiver.ID,
		Type:        "collect",
		Point:       point,
		Description: "ได้รับแต้มจากคุณ" + user.Firstname + " " + user.Lastname,
	})

	// CreateNotification to notification service
	go u.broker.CreateNotification(dm.NotificationCreatedEvent{
		UserID: user.ID,
		Title:  "มอบแต้ม",
		Body:   "มอบแต้มให้คุณ" + receiver.Firstname + " " + receiver.Lastname + " จำนวน " + fmt.Sprintf("%.2f", point) + " แต้ม",
	})

	// CreateNotification to notification service
	go u.broker.CreateNotification(dm.NotificationCreatedEvent{
		UserID: receiver.ID,
		Title:  "ทิ้งขยะ",
		Body:   "ได้รับแต้มจากคุณ" + user.Firstname + " " + user.Lastname + " จำนวน " + fmt.Sprintf("%.2f", point) + " แต้ม",
	})

	return nil
}
