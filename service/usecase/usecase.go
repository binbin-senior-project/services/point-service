package usecase

// RegisterUsecase struct
type RegisterUsecase struct {
	Analytic IAnalyticUsecase
	Bin      IBinUsecase
	Trash    ITrashUsecase
	User     IUserUsecase
}
