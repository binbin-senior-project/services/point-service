package usecase

import (
	amqp "gitlab.com/mangbinbin/services/point-service/service/delivery/amqp"
	dm "gitlab.com/mangbinbin/services/point-service/service/domain"
	r "gitlab.com/mangbinbin/services/point-service/service/repository"
)

// IBinUsecase interface
type IBinUsecase interface {
	Store(bin dm.Bin) error
	Fetch(offset int, limit int) ([]dm.Bin, error)
	GetByID(id string) (dm.Bin, error)
	Update(bin dm.Bin) error
	Delete(id string) error
}

// BinUsecase is an object
type BinUsecase struct {
	repo   r.RegisterRepository
	broker *amqp.AMQPPublish
}

// NewBinUsecase is an inital method for TrashRepository struct
// Called by Client (main.go)
func NewBinUsecase(repo r.RegisterRepository, broker *amqp.AMQPPublish) IBinUsecase {
	return &BinUsecase{
		repo:   repo,
		broker: broker,
	}
}

// Store method is a method
func (u *BinUsecase) Store(bin dm.Bin) error {

	err := u.repo.Bin.Store(bin)

	return err
}

// Fetch method is a method
func (u *BinUsecase) Fetch(offset int, limit int) ([]dm.Bin, error) {

	bins, err := u.repo.Bin.Fetch(offset, limit)

	return bins, err
}

// GetByID method is a method
func (u *BinUsecase) GetByID(id string) (dm.Bin, error) {

	bin, err := u.repo.Bin.GetByID(id)

	return bin, err
}

// Update is a method
func (u *BinUsecase) Update(bin dm.Bin) error {

	err := u.repo.Bin.Update(bin)

	return err
}

// Delete is a method
func (u *BinUsecase) Delete(id string) error {

	err := u.repo.Bin.Delete(id)

	return err
}
