package usecase

import (
	amqp "gitlab.com/mangbinbin/services/point-service/service/delivery/amqp"
	r "gitlab.com/mangbinbin/services/point-service/service/repository"
)

// IAnalyticUsecase interface
type IAnalyticUsecase interface {
	GetNewTrashToday() (int, error)
}

// AnalyticUsecase is an object
type AnalyticUsecase struct {
	repo   r.RegisterRepository
	broker *amqp.AMQPPublish
}

// NewAnalyticUsecase is an inital method for TrashRepository struct
// Called by Client (main.go)
func NewAnalyticUsecase(repo r.RegisterRepository, broker *amqp.AMQPPublish) IAnalyticUsecase {
	return &AnalyticUsecase{
		repo:   repo,
		broker: broker,
	}
}

// GetNewTrashToday method is a method
func (u *AnalyticUsecase) GetNewTrashToday() (int, error) {

	count, err := u.repo.Analytic.GetNewTrashToday()

	if err != nil {
		return 0, err
	}

	return count, nil
}
