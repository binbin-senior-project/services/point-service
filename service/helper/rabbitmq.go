package helper

import (
	"encoding/gob"
	"bytes"
)

func SerializePayload(payload interface{}) ([]byte, error) {
	b := bytes.Buffer{}
    e := gob.NewEncoder(&b)
	err := e.Encode(payload)
	
    if err != nil {
		return []byte{}, err
	}

	return b.Bytes(), nil
}

func DeserializePayload(model interface{}, message []byte) error {
	b := bytes.Buffer{}
	b.Write(message)
	de := gob.NewDecoder(&b)
	err := de.Decode(model)

	if err != nil {
		return err
	}

	return nil
}