package domain

import "time"

// Bin struct
type Bin struct {
	ID          string
	Name        string
	Description string
	Status      string
	Peripheral  string
	Latitude    float64
	Longitude   float64
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

// Trash struct
type Trash struct {
	ID           string
	Name         string
	Code         string
	RegularPoint float64
	SalePoint    float64
}

// User struct
type User struct {
	ID        string
	Firstname string
	Lastname  string
	Phone     string
	Point     float64
}

// Collect struct
type Collect struct {
	ID      string
	TrashID string
	UserID  string
}
