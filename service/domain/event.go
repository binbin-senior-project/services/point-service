package domain

// UserCreatedEvent struct
type UserCreatedEvent struct {
	UserID    string
	Firstname string
	Lastname  string
	Phone     string
}

// UserUpdatedEvent struct
type UserUpdatedEvent struct {
	UserID    string
	Firstname string
	Lastname  string
	Photo     string
}

// PointUpdatedEvent struct
type PointUpdatedEvent struct {
	UserID string
	Point  float64
}

// UserTransactionCreatedEvent struct
type UserTransactionCreatedEvent struct {
	UserID      string
	Type        string
	Point       float64
	Description string
}

// NotificationCreatedEvent struct
type NotificationCreatedEvent struct {
	UserID string
	Title  string
	Body   string
}
