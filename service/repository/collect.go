package repository

import (
	"database/sql"

	dm "gitlab.com/mangbinbin/services/point-service/service/domain"
)

// ICollectRepository interface
type ICollectRepository interface {
	Store(collect dm.Collect) error
}

// CollectRepository is an object
type CollectRepository struct {
	db *sql.DB
}

// NewCollectRepository is an inital method
func NewCollectRepository(db *sql.DB) ICollectRepository {
	return &CollectRepository{
		db: db,
	}
}

// Store method is a method
func (r *CollectRepository) Store(collect dm.Collect) error {
	query := `
		INSERT INTO collects (user_id, trash_id)
		VALUES ($1, $2)
	`

	_, err := r.db.Query(
		query,
		collect.UserID,
		collect.TrashID,
	)

	return err
}
