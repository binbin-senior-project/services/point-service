package repository

import (
	"database/sql"

	dm "gitlab.com/mangbinbin/services/point-service/service/domain"
)

// IUserRepository interface
type IUserRepository interface {
	Store(user dm.User) error
	GetByID(id string) (dm.User, error)
	GetByPhone(phone string) (dm.User, error)
	Update(user dm.User) error
	UpdateByKey(id string, key string, value string) error
}

// UserRepository is an object
type UserRepository struct {
	db *sql.DB
}

// NewUserRepository is an inital method
func NewUserRepository(db *sql.DB) IUserRepository {
	return &UserRepository{
		db: db,
	}
}

// Store method is a method
func (r *UserRepository) Store(user dm.User) error {

	query := `
		INSERT INTO users (id, user_firstname, user_lastname, user_phone)
		VALUES ($1, $2, $3, $4)
	`

	_, err := r.db.Query(
		query,
		user.ID,
		user.Firstname,
		user.Lastname,
		user.Phone,
	)

	return err
}

// GetByID method is a method
func (r *UserRepository) GetByID(id string) (dm.User, error) {

	query := `
		SELECT id, user_firstname, user_lastname, user_phone, user_point
		FROM users WHERE id = $1
	`

	row := r.db.QueryRow(query, id)

	user := dm.User{}

	err := row.Scan(
		&user.ID,
		&user.Firstname,
		&user.Lastname,
		&user.Phone,
		&user.Point,
	)

	if err != nil {
		return dm.User{}, err
	}

	return user, nil
}

// GetByPhone method is a method
func (r *UserRepository) GetByPhone(phone string) (dm.User, error) {

	query := `
		SELECT id, user_firstname, user_lastname, user_phone, user_point
		FROM users WHERE user_phone = $1
	`

	row := r.db.QueryRow(query, phone)

	user := dm.User{}

	err := row.Scan(
		&user.ID,
		&user.Firstname,
		&user.Lastname,
		&user.Phone,
		&user.Point,
	)

	if err != nil {
		return dm.User{}, err
	}

	return user, nil
}

// Update is a method
func (r *UserRepository) Update(user dm.User) error {

	query := `
		UPDATE users SET user_firstname=$2, user_lastname=$3, user_phone=$4 WHERE id = $1
	`

	_, err := r.db.Query(
		query,
		user.ID,
		user.Firstname,
		user.Lastname,
		user.Phone,
	)

	return err
}

// UpdateByKey is a method
func (r *UserRepository) UpdateByKey(id string, key string, value string) error {
	query := `UPDATE users SET ` + key + `=$2 WHERE id = $1`

	_, err := r.db.Query(query, id, value)

	return err
}
