package repository

import (
	"database/sql"

	"github.com/google/uuid"
	dm "gitlab.com/mangbinbin/services/point-service/service/domain"
)

// IBinRepository interface
type IBinRepository interface {
	Store(bin dm.Bin) error
	Fetch(offset int, limit int) ([]dm.Bin, error)
	GetByID(id string) (dm.Bin, error)
	Update(bin dm.Bin) error
	Delete(id string) error
}

// BinRepository is an object
type BinRepository struct {
	db *sql.DB
}

// NewBinRepository is an inital method
func NewBinRepository(db *sql.DB) IBinRepository {
	return &BinRepository{
		db: db,
	}
}

// Store method is a method
func (r *BinRepository) Store(bin dm.Bin) error {
	query := `
		INSERT INTO bins (id, bin_name, bin_description, bin_status, bin_peripheral, bin_latitude, bin_longitude)
		VALUES ($1, $2, $3, $4, $5, $6, $7)
	`

	bin.ID = uuid.New().String()

	_, err := r.db.Query(
		query,
		bin.ID,
		bin.Name,
		bin.Description,
		bin.Status,
		bin.Peripheral,
		bin.Latitude,
		bin.Longitude,
	)

	return err
}

// Fetch method is a method
func (r *BinRepository) Fetch(offset int, limit int) ([]dm.Bin, error) {

	query := `
		SELECT id, bin_name, bin_description, bin_status, bin_peripheral, bin_latitude, bin_longitude
		FROM bins OFFSET $1 LIMIT $2
	`

	rows, err := r.db.Query(query, offset, limit)

	if err != nil {
		return nil, err
	}

	bins := make([]dm.Bin, 0)

	for rows.Next() {
		bin := dm.Bin{}

		err = rows.Scan(
			&bin.ID,
			&bin.Name,
			&bin.Description,
			&bin.Status,
			&bin.Peripheral,
			&bin.Latitude,
			&bin.Longitude,
		)

		if err != nil {
			return nil, err
		}

		bins = append(bins, bin)
	}

	return bins, nil
}

// GetByID method is a method
func (r *BinRepository) GetByID(id string) (dm.Bin, error) {

	query := `
		SELECT id, bin_name, bin_description, bin_status, bin_peripheral, bin_latitude, bin_longitude
		FROM bins WHERE id = $1
	`

	row := r.db.QueryRow(query, id)

	bin := dm.Bin{}

	err := row.Scan(
		&bin.ID,
		&bin.Name,
		&bin.Description,
		&bin.Status,
		&bin.Peripheral,
		&bin.Latitude,
		&bin.Longitude,
	)

	if err != nil {
		return dm.Bin{}, err
	}

	return bin, nil
}

// Update is a method
func (r *BinRepository) Update(bin dm.Bin) error {

	query := `
		UPDATE bins SET bin_name=$2, bin_description=$3, bin_status=$4, bin_peripheral=$5, bin_latitude=$6, bin_longitude=$7 WHERE id = $1
	`

	_, err := r.db.Query(
		query,
		bin.ID,
		bin.Name,
		bin.Description,
		bin.Status,
		bin.Peripheral,
		bin.Latitude,
		bin.Longitude,
	)

	if err != nil {
		return err
	}

	return nil
}

// Delete is a method
func (r *BinRepository) Delete(id string) error {

	query := `
		DELETE FROM bins WHERE id = $1
	`

	_, err := r.db.Query(query, id)

	return err
}
