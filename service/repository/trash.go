package repository

import (
	"database/sql"

	"github.com/google/uuid"
	dm "gitlab.com/mangbinbin/services/point-service/service/domain"
)

// ITrashRepository interface
type ITrashRepository interface {
	Store(trash dm.Trash) error
	Fetch(offset int, limit int) ([]dm.Trash, error)
	GetByID(id string) (dm.Trash, error)
	GetByCode(code string) (dm.Trash, error)
	Update(trash dm.Trash) error
	Delete(id string) error
}

// TrashRepository is an object
type TrashRepository struct {
	db *sql.DB
}

// NewTrashRepository is an inital method
func NewTrashRepository(db *sql.DB) ITrashRepository {
	return &TrashRepository{
		db: db,
	}
}

// Store method is a method
func (r *TrashRepository) Store(trash dm.Trash) error {

	query := `
		INSERT INTO trashes (id, trash_name, trash_code, trash_regular_point, trash_sale_point)
		VALUES ($1, $2, $3, $4, $5)
	`

	trash.ID = uuid.New().String()

	_, err := r.db.Query(
		query,
		trash.ID,
		trash.Name,
		trash.Code,
		trash.RegularPoint,
		trash.SalePoint,
	)

	return err
}

// Fetch method is a method
func (r *TrashRepository) Fetch(offset int, limit int) ([]dm.Trash, error) {

	query := `
		SELECT id, trash_name, trash_code, trash_regular_point, trash_sale_point
		FROM trashes OFFSET $1 LIMIT $2
	`

	rows, err := r.db.Query(query, offset, limit)

	if err != nil {
		return nil, err
	}

	trashes := make([]dm.Trash, 0)

	for rows.Next() {
		trash := dm.Trash{}

		err = rows.Scan(
			&trash.ID,
			&trash.Name,
			&trash.Code,
			&trash.RegularPoint,
			&trash.SalePoint,
		)

		if err != nil {
			return nil, err
		}

		trashes = append(trashes, trash)
	}

	return trashes, nil
}

// GetByID method is a method
func (r *TrashRepository) GetByID(id string) (dm.Trash, error) {

	query := `
		SELECT id, trash_name, trash_code, trash_regular_point, trash_sale_point
		FROM trashes WHERE id = $1
	`

	row := r.db.QueryRow(query, id)

	trash := dm.Trash{}

	err := row.Scan(
		&trash.ID,
		&trash.Name,
		&trash.Code,
		&trash.RegularPoint,
		&trash.SalePoint,
	)

	if err != nil {
		return dm.Trash{}, err
	}

	return trash, nil
}

// GetByCode method is a method
func (r *TrashRepository) GetByCode(code string) (dm.Trash, error) {

	query := `
		SELECT id, trash_name, trash_code, trash_regular_point, trash_sale_point
		FROM trashes WHERE trash_code = $1
	`

	row := r.db.QueryRow(query, code)

	trash := dm.Trash{}

	err := row.Scan(
		&trash.ID,
		&trash.Name,
		&trash.Code,
		&trash.RegularPoint,
		&trash.SalePoint,
	)

	if err != nil {
		return dm.Trash{}, err
	}

	return trash, nil
}

// Update is a method
func (r *TrashRepository) Update(trash dm.Trash) error {

	query := `
		UPDATE trashes SET trash_name=$2, trash_code=$3, trash_regular_point=$4, trash_sale_point=$5 WHERE id = $1
	`

	_, err := r.db.Query(
		query,
		trash.ID,
		trash.Name,
		trash.Code,
		trash.RegularPoint,
		trash.SalePoint,
	)

	if err != nil {
		return err
	}

	return nil
}

// Delete is a method
func (r *TrashRepository) Delete(id string) error {

	query := `
		DELETE FROM trashes WHERE id = $1
	`

	_, err := r.db.Query(query, id)

	return err
}
