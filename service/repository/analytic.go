package repository

import (
	"database/sql"
)

// IAnalyticRepository interface
type IAnalyticRepository interface {
	GetNewTrashToday() (int, error)
}

// AnalyticRepository is an object
type AnalyticRepository struct {
	db *sql.DB
}

// NewAnalyticRepository is an inital method
func NewAnalyticRepository(db *sql.DB) IAnalyticRepository {
	return &AnalyticRepository{
		db: db,
	}
}

// GetNewTrashToday method
func (r *AnalyticRepository) GetNewTrashToday() (int, error) {
	query := `SELECT COUNT(*) FROM collects WHERE DATE("created_at") = current_date`

	row := r.db.QueryRow(query)

	var count int

	err := row.Scan(
		&count,
	)

	if err != nil {
		return 0, err
	}

	return count, nil
}
