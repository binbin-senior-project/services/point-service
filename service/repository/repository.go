package repository

// RegisterRepository struct
type RegisterRepository struct {
	Analytic IAnalyticRepository
	Bin      IBinRepository
	Trash    ITrashRepository
	User     IUserRepository
	Collect  ICollectRepository
}
